********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Filesystem Backup (fbu) module
Author: Robert Douglass <rob at robshouse dot net>
Last update: (See CHANGELOG for details)
Drupal: CVS
Dependencies:
  * PEAR File_Archive: http://pear.php.net/package/File_Archive
  * PEAR MIME_Type: http://pear.php.net/package/MIME_Type
  * Crontabs

DESCRIPTION
********************************************************************
The Filesystem Backup module (fbu) simplifies the administrator's 
job of keeping an offsite backup of the files that make up a Drupal 
site. The module will send the administrator a series of uniquely 
numbered archive files (zip, bz2, tgz etc.) that together make a 
complete copy of your Drupal files.

FEATURES
********************************************************************
Mails an entire backup of the document root to the administrator in 
chunks. The maximum size of each chunk is configurable so that 
restrictions on email attachment size can be observed.

Works in batches (cron) so that the drain on system resources can be 
controlled. The frequency of the backup process is configurable.

Can archive files to many popular (zip, bz2, tgz) archive types.

Can be configured to backup the entire filesystem (complete mode), 
or only those files that are new or modified since the last backup 
(only new mode).


DEPENDENCIES
********************************************************************
  * PEAR File_Archive: http://pear.php.net/package/File_Archive
  * PEAR MIME_Type: http://pear.php.net/package/MIME_Type
  * Crontabs

INSTALLATION
********************************************************************
See the INSTALL.txt file in this directory.

WISH LIST
********************************************************************
  * A way to include/exclude specific directories
  * Special treatment for the settings folder so that only the current
    site's settings.php + modules + themes etc. get included
  * Performance improvements. The current implementation is limited
    to 50 files per run because the scripts take too long otherwise
  * Timeout detection; read the max execution time, time the script's
    runtime, and stop before danger of timing out.
  * Copy new/modified archives to an FTP directory instead of emailing 
    them.
  * FTP new/modified archives to a remote server.
  * Have the option of using Unix commands like cp to do the directory
    scanning and file copying.

HOW TO USE THIS MODULES ONCE INSTALLED
********************************************************************
Read this after installing the module.

  1. Navigate to admin/settings/fbu. Here you will be shown an error
     message if the File_Archive library hasn't been installed properly.
  2. Set all configuration options to your liking and press "Save 
     Configuration".
  3. As long as the field "Automatically backup files every" is set
     to something larger that 0 seconds, backups will start appearing
     in your mailbox every cron run. The backups will be numbered
     sequentially and each one will contain only a portion of the
     entire backup. To restore a site from the backups files, it 
     would be necessary to extract all of them into the same directory,
     in the order that they were received. 
  4. Any break in the sequential numbering of the backup files indicates
     an incomplete backup (due to some sort of failure). If you have
     an incomplete set of backup files, it is recommended that you 
     change the backups to complete mode and initiate a complete backup.
  5. A complete backup will generally involve 30 or more chunks, 
     which in turn translates to 30 or more cron runs and emails.
     If your crontab is set for every hour, then this will obviously
     take more than a day to complete. After completing a complete
     backup, the fbu module will then proceed to monitor your filesystem
     for new or modified files and will mail these to you.
